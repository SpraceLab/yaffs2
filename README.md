# YAFFS2: Direct Interface

This project maintain the YAFFS2 source code modified to use the Direct Interface (`master_direct` branch). The `master` branch is a copy of the original repo at [www.aleph1.co.uk/yaffs2](www.aleph1.co.uk/yaffs2).

The modification is made following instructions provided by the authors in `direct/README.txt`.


```
$ cd direct
$ ./handle_common.sh copy
$ cd ..
$ rm *[ch]
$ rm -rf Kconfig_multi Kconfig_single linux-tests Makefile Makefile.kernel mtdemul patches README-linux rtems utils
$ rm .gitignore
```

**NOTE**: `.gitignore` must be deleted because it prevents to detected the files copied by `handle_common.sh`